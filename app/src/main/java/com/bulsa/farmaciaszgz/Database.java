package com.bulsa.farmaciaszgz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by borja on 20/12/2016.
 */

public class Database extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "farmacias.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLA_FAVORITOS = "favoritos";
    public Database(Context contexto) {
        super(contexto, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLA_FAVORITOS + "( nombre TEXT, direccion TEXT, url TEXT, telefono TEXT, latitud TXT, longitud TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS favoritos");
        onCreate(db);
    }

    public void eliminarFarmacia(Farmacia f) {

        SQLiteDatabase db = getWritableDatabase();

        String[] argumentos = new String[]{f.getNombre()};
        db.delete(TABLA_FAVORITOS, "nombre = ?", argumentos);
        db.close();
    }
    public void nuevaF(Farmacia f) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("nombre", f.getNombre());
        values.put("direccion",f.getDireccion());
        values.put("url", f.getUrl());
        values.put("telefono",f.getTelefono());
        values.put("longitud",f.getLongitud()+"");
        values.put("latitud",f.getLatitud()+"");
        db.insertOrThrow(TABLA_FAVORITOS, null, values);
        db.close();
    }
    public ArrayList<Farmacia> getFarmacias() {

        final String[] SELECT = {"nombre", "direccion", "url", "telefono",
                "longitud", "latitud"};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLA_FAVORITOS, SELECT, null, null, null, null,null);

        ArrayList<Farmacia> listaF = new ArrayList<>();
        Farmacia f = null;
        while (cursor.moveToNext()) {
            f = new Farmacia();
            f.setNombre(cursor.getString(0));
            f.setDireccion(cursor.getString(1));
            f.setUrl(cursor.getString(2));
            f.setTelefono(cursor.getString(3));
            f.setLongitud(Float.parseFloat(cursor.getString(4)));
            f.setLatitud(Float.parseFloat(cursor.getString(5)));
            listaF.add(f);
        }
        db.close();

        return listaF;

    }

}
