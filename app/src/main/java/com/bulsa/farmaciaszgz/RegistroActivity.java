package com.bulsa.farmaciaszgz;


import android.app.Activity;
import android.os.AsyncTask;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.bulsa.farmaciaszgz.util.Constantes.SERVER_URL;

public class RegistroActivity extends Activity {
    private Button a;
    private Button c;
    private EditText nombre;
    private EditText email;
    private EditText pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        a=(Button)findViewById(R.id.btAceptarR);
        c=(Button)findViewById(R.id.btCancelar);
        nombre=(EditText)findViewById(R.id.etUsuarioR);
        email=(EditText)findViewById(R.id.etEmailR);
        pass=(EditText)findViewById(R.id.etPassR);
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebService webService = new WebService();
                webService.execute(nombre.getText().toString(),email.getText().toString(),pass.getText().toString());

            }
        });
        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private class WebService extends AsyncTask<String, Void, Void> {


        @Override
        protected Void doInBackground(String... params) {

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getForObject(SERVER_URL + "/add_usuario?nombre=" + params[0] + "&email=" + params[1] + "&pass=" + params[2], Boolean.class);

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Void aVoid) {

            nombre.setText("");
            email.setText("");
            pass.setText("");

        }
    }
}
