package com.bulsa.farmaciaszgz.util;

import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

/**
 * Created by borja on 20/12/2016.
 */

public class Util {
    public static LatLng DeUMTSaLatLng(double este, double oeste) {

        UTMRef utm = new UTMRef(este, oeste, 'N', 30);

        return utm.toLatLng();
    }
}
