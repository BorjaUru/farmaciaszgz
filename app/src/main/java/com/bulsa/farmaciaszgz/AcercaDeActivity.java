package com.bulsa.farmaciaszgz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import static com.bulsa.farmaciaszgz.util.Constantes.VERSION;

public class AcercaDeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerca_de);
        TextView version=(TextView)findViewById(R.id.tvVersion);
        version.setText(R.string.version+ VERSION);
    }
}
