package com.bulsa.farmaciaszgz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.bulsa.farmaciaszgz.util.Constantes.SERVER_URL;

public class AnadirComentariosActivity extends AppCompatActivity {
    private Button aceptar;
    private Button cancelar;
    private EditText nota;
    private EditText descripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anadir_comentarios);
        Intent intent = getIntent();
       final  String nombre = intent.getStringExtra("nombre");
        aceptar=(Button)findViewById(R.id.btAceptarC);
        nota=(EditText)findViewById(R.id.editText3);
        cancelar=(Button)findViewById(R.id.btCancelarC);
        descripcion=(EditText)findViewById(R.id.editText2);
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String des = descripcion.getText().toString();
                String not= nota.getText().toString();
                WebService webService = new WebService();
                webService.execute(nombre,des,not);
            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private class WebService extends AsyncTask<String, Void, Void> {

        private ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... params) {

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getForObject(SERVER_URL + "//add_comentario?nombre=" + params[0] + "&descripcion=" + params[1] + "&nota=" + params[2], Void.class);

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AnadirComentariosActivity.this);
            dialog.setTitle(R.string.enviado);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            nota.setText("");
            descripcion.setText("");

            if (dialog != null)
                dialog.dismiss();
        }
    }
}
