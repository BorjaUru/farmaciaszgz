package com.bulsa.farmaciaszgz;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import static com.bulsa.farmaciaszgz.ListaFarmaciasActivity.listaff;
import static com.bulsa.farmaciaszgz.ListaFarmaciaFActivity.listaf;

public class MapaActivity extends AppCompatActivity {
    MapView mapaView;
    private MapboxMap mapa;
    private FloatingActionButton btUbicacion;
    private LocationServices servicioUbicacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapboxAccountManager.start(this, "pk.eyJ1IjoiYnVsc2EiLCJhIjoiY2l2dHV4bjI1MDAyeDJvcDhnanZ2enp1OCJ9.lgsxLGoptlqdGCGrB2i4jQ");
        setContentView(R.layout.activity_mapa);
        mapaView = (MapView) findViewById(R.id.mapaView);
        mapaView.onCreate(savedInstanceState);
        Intent intent = getIntent();

        String nombre = intent.getStringExtra("favoritos");

        servicioUbicacion = LocationServices.getLocationServices(this);


        if(nombre.equals("favoritos")){
            for(Farmacia f:listaf){
                marcarUbicacion(f.getNombre(),f.getLatitud(),f.getLongitud());
            }
        }else{
            for(Farmacia f: listaff){
                marcarUbicacion(f.getNombre(),f.getLatitud(),f.getLongitud());
            }
        }



    }

    private void marcarUbicacion(final String nombre,
                                 final double latitud, final double longitud) {

        mapaView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapa = mapboxMap;
                mapboxMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitud, longitud))
                        .title(nombre));

                CameraPosition position = new CameraPosition.Builder()
                        .target(new LatLng(latitud, longitud)) // Sets the new camera position
                        .zoom(10) // Sets the zoom
                        .tilt(30) // Set the camera tilt
                        .build(); // Creates a CameraPosition from the builder

                mapboxMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(position), 7000);
            }
        });
    }
}

