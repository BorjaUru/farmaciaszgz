package com.bulsa.farmaciaszgz;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class ListaFarmaciaFActivity extends AppCompatActivity {
    private ListView lista;
    public static ArrayList<Farmacia> listaf;
    public static FarmaciaAdapter adaptador;
    public static FarmaciaAdapter adaptadorr;

    private Database d;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_farmacia_f);
        lista = (ListView) findViewById(R.id.lFarmacias);
        listaf=new ArrayList<>();
        d=new Database(this);
        actualizarSQL();
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Farmacia f=(Farmacia)adaptador.getItem(i);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(f.getUrl()));
                startActivity(intent);
            }
        });
        registerForContextMenu(lista);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_lista_f_farmacias, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.cerrar:
                intent= new Intent(ListaFarmaciaFActivity.this, MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.informacion:
                intent= new Intent(ListaFarmaciaFActivity.this, AcercaDeActivity.class);
                startActivity(intent);
                return true;
            case R.id.listaaaaaaaaaaaaaaaaaaaaaaaa:
                intent= new Intent(ListaFarmaciaFActivity.this, ListaFarmaciasActivity.class);
                startActivity(intent);
                return  true;
            case R.id.mapas:
                intent= new Intent(ListaFarmaciaFActivity.this, MapaActivity.class);
                intent.putExtra("favoritos","favoritos");
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getMenuInflater().inflate(R.menu.contex_item_lista_farmacias2, menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int itemSeleccionado = info.position;

        Farmacia f;
        switch (item.getItemId()) {
            case R.id.Cmapa:
                Intent intentMapa = new Intent(this, Mapa1FActivity.class);
                f=listaf.get(itemSeleccionado);
                intentMapa.putExtra("nombre", f.getNombre());
                intentMapa.putExtra("latitud", f.getLatitud());
                intentMapa.putExtra("longitud", f.getLongitud());
                startActivity(intentMapa);
                break;
            case R.id.Cfavoritoss:
                f=listaf.get(itemSeleccionado);
                d.eliminarFarmacia(f);
                actualizarSQL();
                break;
            default:
                break;
        }
        return false;
    }



    @Override
    protected void onResume() {
        super.onResume();
        actualizarSQL();
    }


    public void actualizarSQL(){
        listaf=d.getFarmacias();
        adaptadorr=new FarmaciaAdapter(this,listaf);
        lista.setAdapter(adaptadorr);

    }
}
