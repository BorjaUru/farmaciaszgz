package com.bulsa.farmaciaszgz;

import android.app.ProgressDialog;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;


import static com.bulsa.farmaciaszgz.util.Constantes.SERVER_URL;

public class ComentariosActivity extends AppCompatActivity {
    private ArrayList<Comentario> listaD;
    private ArrayList<Comentario> lista ;
    private ComentarioAdapter adaptador;
    private String nombre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentarios);
        Intent intent = getIntent();
        nombre = intent.getStringExtra("nombre");
        listaD=new ArrayList<>();
        lista = new ArrayList<>();
        ComentarioAdapter adaptador = new ComentarioAdapter(this, listaD);
        WebService webService = new WebService();
        webService.execute();
        ListView lvLista = (ListView) findViewById(R.id.lComentarios);
        lvLista.setAdapter(adaptador);
    }
    private class WebService extends AsyncTask<String, Void, Void> {

        private ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... params) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            Comentario[] ComentariosArray = restTemplate.getForObject(SERVER_URL + "/comentarios", Comentario[].class);
            lista.addAll(Arrays.asList(ComentariosArray ));

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            lista.clear();

            dialog = new ProgressDialog(ComentariosActivity.this);
            dialog.setTitle(R.string.cargandoC);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (dialog != null)
                dialog.dismiss();
            for(Comentario c:lista){
                if(c.getNombre().equals(nombre)){
                    listaD.add(c);
                }
            }
            adaptador.notifyDataSetChanged();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

        }
    }
}
