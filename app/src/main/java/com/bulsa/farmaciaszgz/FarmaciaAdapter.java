package com.bulsa.farmaciaszgz;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by borja on 18/12/2016.
 */

public class FarmaciaAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Farmacia> listaFarmacia;
    private LayoutInflater inflater;

    public FarmaciaAdapter(Activity context, ArrayList<Farmacia> listaFarmacia) {
        this.context = context;
        this.listaFarmacia = listaFarmacia;
        inflater = LayoutInflater.from(context);
    }

    static class ViewHolder {
        TextView nombre;
        TextView direccion;
        TextView url;
        TextView telefono;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_lista_farmacias, null);

            holder = new ViewHolder();
            holder.nombre = (TextView) convertView.findViewById(R.id.tvNombre);
            holder.direccion = (TextView) convertView.findViewById(R.id.tvDireccion);
            holder.telefono = (TextView) convertView.findViewById(R.id.tvTelefono);
            holder.url = (TextView) convertView.findViewById(R.id.tvUrl);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Farmacia f = listaFarmacia.get(position);
        holder.url.setText(f.getUrl());
        holder.telefono.setText(f.getTelefono());
        holder.nombre.setText(f.getNombre());
        holder.direccion.setText(f.getDireccion());
        return convertView;
    }

    @Override
    public int getCount() {
        return listaFarmacia.size();
    }

    @Override
    public Object getItem(int posicion) {
        return listaFarmacia.get(posicion);
    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }

}
