package com.bulsa.farmaciaszgz;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

public class Mapa1FActivity extends AppCompatActivity {
    MapView mapaView;
    private MapboxMap mapa;
    private FloatingActionButton btUbicacion;
    private LocationServices servicioUbicacion;
    private double latitud;
    private double longitud;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapboxAccountManager.start(this, "pk.eyJ1IjoiYnVsc2EiLCJhIjoiY2l2dHV4bjI1MDAyeDJvcDhnanZ2enp1OCJ9.lgsxLGoptlqdGCGrB2i4jQ");
        setContentView(R.layout.activity_mapa1_f);
        mapaView = (MapView) findViewById(R.id.mapaView2);
        mapaView.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String nombre = intent.getStringExtra("nombre");


        servicioUbicacion = LocationServices.getLocationServices(this);

        latitud = intent.getDoubleExtra("latitud", 0);
        longitud = intent.getDoubleExtra("longitud", 0);
        marcarUbicacion(nombre, latitud, longitud);



    }

    private void marcarUbicacion(final String nombre,
                                 final double latitud, final double longitud) {

        Toast.makeText(this, latitud + " " + longitud, Toast.LENGTH_SHORT).show();
        mapaView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapa = mapboxMap;
                mapboxMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitud, longitud))
                        .title(nombre));

                CameraPosition position = new CameraPosition.Builder()
                        .target(new LatLng(latitud, longitud)) // Sets the new camera position
                        .zoom(17) // Sets the zoom
                        .tilt(30) // Set the camera tilt
                        .build(); // Creates a CameraPosition from the builder

                mapboxMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(position), 7000);
            }
        });
    }
}
