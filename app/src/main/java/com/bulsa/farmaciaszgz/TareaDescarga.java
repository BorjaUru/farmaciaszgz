package com.bulsa.farmaciaszgz;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.bulsa.farmaciaszgz.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import uk.me.jstott.jcoord.LatLng;

import static com.bulsa.farmaciaszgz.ListaFarmaciasActivity.adaptador;
import static com.bulsa.farmaciaszgz.ListaFarmaciasActivity.listaff;
import static com.bulsa.farmaciaszgz.util.Constantes.URL;

/**
 * Created by borja on 19/12/2016.
 */

public class TareaDescarga extends AsyncTask<String,Void,Void> {
    private boolean error = false;
    private ProgressDialog dialog;
    private ListaFarmaciasActivity activity;
    public TareaDescarga(ListaFarmaciasActivity activity){
        this.activity=activity;
    }
    @Override
    protected Void doInBackground(String... strings) {
        InputStream is = null;
        String resultado = null;
        JSONObject json = null;
        JSONArray jsonArray = null;

        try {
            URL url = new URL(URL);
            HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String linea = null;

            while ((linea = br.readLine()) != null)
                sb.append(linea + "\n");

            conexion.disconnect();
            br.close();
            resultado = sb.toString();

            json = new JSONObject(resultado);
            jsonArray = json.getJSONArray("features");

            String nombre = null;
            String urll=null;
            String coordenadas = null;
            Farmacia f = null;
            for (int i = 0; i < jsonArray.length(); i++) {
                nombre = jsonArray.getJSONObject(i).getJSONObject("properties").getString("title");
                urll=jsonArray.getJSONObject(i).getJSONObject("properties").getString("link");
                String descripcion=jsonArray.getJSONObject(i).getJSONObject("properties").getString("description");
                coordenadas = jsonArray.getJSONObject(i).getJSONObject("geometry").getString("coordinates");
                coordenadas = coordenadas.substring(1, coordenadas.length() - 1);
                String arrayCoordenadas[] = coordenadas.split(",");
                LatLng latLng = Util.DeUMTSaLatLng(
                        Double.parseDouble(arrayCoordenadas[0]),
                        Double.parseDouble(arrayCoordenadas[1]));
                String a[]=descripcion.split("Teléfono:");
                f = new Farmacia();
                f.setNombre(nombre);
                f.setDireccion(a[0]);
                f.setTelefono(a[1].substring(0,9));
                f.setUrl(urll);
                f.setLatitud(latLng.getLat());
                f.setLongitud(latLng.getLng());
                listaff.add(f);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            error = true;
        } catch (JSONException jse) {
            jse.printStackTrace();
            error = true;
        }

        return null;
    }
    @Override
    protected void onCancelled() {
        super.onCancelled();
        listaff = new ArrayList<>();
    }
    @Override
    protected void onProgressUpdate(Void... progreso) {
        super.onProgressUpdate(progreso);
        adaptador.notifyDataSetChanged();
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        dialog = new ProgressDialog(activity);
        dialog.setTitle(R.string.cargando);
        dialog.show();
    }

    @Override
    protected void onPostExecute(Void resultado) {
        super.onPostExecute(resultado);

        if (error) {
            Toast.makeText(activity,R.string.error, Toast.LENGTH_SHORT).show();
            return;
        }

        if (dialog != null)
            dialog.dismiss();

        adaptador.notifyDataSetChanged();
    }
}
