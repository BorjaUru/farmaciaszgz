package com.bulsa.farmaciaszgz;

import android.content.SharedPreferences;
import android.preference.PreferenceActivity;

import android.os.Bundle;
import android.preference.PreferenceManager;

public class PreferenciasActivity extends PreferenceActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.preferencias);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    public void actualizar(){
        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(this);
        boolean verFavoritos = preferencias.getBoolean("opcion_ver_favoritos", false);
    }
}
